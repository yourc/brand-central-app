import React, { Component } from 'react';
import Masonry from 'react-masonry-component';

const hrStyle = {
    marginTop: 75,
};

const masonryOptions = {
    transitionDuration: 0
};

class ColourModule extends Component {
    lightOrDark = color => {
        let r, g, b, hsp

        if (color.match(/^rgb/)) {
            color = color.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+(?:\.\d+)?))?\)$/)

            r = color[1]
            g = color[2]
            b = color[3]
        } else {
            color = +("0x" + color.slice(1).replace(
                color.length < 5 && /./g, '$&$&'))

            r = color >> 16
            g = color >> 8 & 255
            b = color & 255
        }

        hsp = Math.sqrt(
            0.299 * (r * r) +
            0.587 * (g * g) +
            0.114 * (b * b)
        )

        if (hsp > 150) {
            return true
        }

        return false
    }

    render() {
        const {
            data,
            primaryColour,
            secondaryColour
        } = this.props

        if(!data.grid_items){
            return (
                <div>
                    You haven't added any grid items here.
                </div>
            )
        }

        const childElements = data.grid_items.map((gridItem, index) => {
            let gridItemClassName = 'grid-item'
            const isLightColour = this.lightOrDark(gridItem.hex_code)

            if(gridItem.primary){
                gridItemClassName += ' primary'
            }

            if(isLightColour){
                gridItemClassName += ' light'
            }

            return (
                <li 
                    key={index} 
                    className={gridItemClassName} 
                    style={{ background: gridItem.hex_code }}
                >
                    <ul>
                        <li>
                            {gridItem.name}
                        </li>
                        <li>
                            {gridItem.hex_code}
                        </li>
                        <li>
                            {gridItem.rgb_code}
                        </li>
                        <li>
                            {gridItem.cmyk_code}
                        </li>
                    </ul>
                </li>
            )
        })

        return (
            <div className="module-container">
                <div>
                    <div className="info">
                        <p style={primaryColour}>
                            {data.grid_info}
                        </p>
                    </div>
                    <div className="colour-grid-container">
                        <Masonry
                            className={'colour-grid'}
                            elementType={'ul'}
                            options={masonryOptions}
                            disableImagesLoaded={false}
                            updateOnEachImageLoad={false} 
                            imagesLoadedOptions={{}}
                        >
                            {childElements}
                        </Masonry>
                        <p style={secondaryColour}>
                            {data.grid_tagline}
                        </p>
                    </div>
                </div>
                { data.file_download &&
                    <div>
                        <a className="button"
                            href={data.button_file}
                            target="_blank"
                        >
                            {data.button_label}
                        </a>
                    </div>
                }
            </div>
        )
    }
}

export default ColourModule;