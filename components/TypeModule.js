import React,  { Component } from 'react';

const hrStyle = {
  marginTop: 75,
};

class TypeModule extends Component {

    render() {

        const {
            data,
            primaryColour,
            secondaryColour
        } = this.props

        return (
            <div className="module-container">
                <ul className="rows">
                    {data.typography_rows && data.typography_rows.map((row, rowIndex) => {
                        
                        return (
                            <li key={rowIndex}>
                                <div className="column left">
                                    <h4 style={{fontFamily: row.font_name}}>
                                        {row.specimen_title}
                                    </h4>
                                    <span className="wyswiwyg"
                                        dangerouslySetInnerHTML={{__html: row.copy}}
                                    />
                                </div>
                                <div className="column right">
                                    <h4 style={{fontFamily: row.font_name}}>
                                        {row.font_name}
                                    </h4>
                                    <p style={{fontFamily: row.font_name}}>
                                        Font Size: {row.font_size}
                                    </p>
                                    <p style={{fontFamily: row.font_name}}>
                                        Line Spacing: {row.line_height}
                                    </p>
                                </div>
                            </li>

                        )
                })}
                </ul>
                {
                    data.file_download ? 
                        <a className="button" 
                            href={data.button_file}
                        >
                            {data.button_label}
                        </a>
                    : null
                }
            </div>
        )
    }
}

export default TypeModule;