import React, { Component } from 'react';
import Error from 'next/error';
import WPAPI from 'wpapi';
import Layout from '../components/Layout';
import Sidebar from '../components/Sidebar';
import Content from '../components/Content';
import Config from '../config';
import Router from 'next/router';

import { cloneDeep } from 'lodash'

const wp = new WPAPI({ endpoint: Config.apiUrl })

wp.clients = wp.registerRoute('wp/v2', '/clients/(?P<id>)')

class Main extends Component {
    scrollContainerRef = null

    state = {
        moduleRefs: {},
        pageVisible: [],
        slug: '',
        isVisible: false,
        scrolledActiveTab: 0,
        userInitiatedActiveTab: null
    }

    static async getInitialProps(context) {
        const { slug } = context.query

        let apiMethod = wp.clients()

        const post = await apiMethod
            .slug(slug)
            .embed()
            .then(data => {
                return data[0];
            });

        return { post };
    }

    componentDidMount() {
        if(typeof window !== 'undefined'){
            window.scrollTo(0, 0)
        }
    }    

    componentDidMount() {
        const token = localStorage.getItem(Config.AUTH_TOKEN) 
     
        if(token) {
            wp.setHeaders('Authorization', `Bearer ${token}`);
            wp.users()
            .me()
            .then(data => {
                const { id } = data;
                this.setState({ id });

                const clients = data.acf.clients 

                const {
                    post
                } = this.props

                if(clients) {
                    const clientId = post.id

                    const hasAccess = clients.findIndex(client => {
                        return client.client.ID === clientId
                    })

                    if(hasAccess != -1) {
                        setTimeout(() => {
                            this.setState({
                                isVisible: true,
                            })
                        },100)

                    } else {
                        const thisSlug = post.slug

                        Router.push({
                            pathname: '/'+thisSlug,
                            query: {access: 'denied'}
                        })
                    }   
                }
            })
        } else {
            const {
                post
            } = this.props

            const thisSlug = post.slug

            Router.push({
                pathname: '/'+thisSlug,
                query: {access: 'denied'}
            })

        }
    }

    getNormalisedActiveTab = (state = this.state) => {
        if(state.userInitiatedActiveTab !== null){
            return state.userInitiatedActiveTab.toString()
        }

        if(state.scrolledActiveTab !== null){
            return state.scrolledActiveTab.toString()
        }

        return null
    }

    setActiveTab = (key, type = 'scroll') => {
        if(type == 'scroll'){
            this.setState({
                scrolledActiveTab: key
            })
        }else{
            this.setState({
                userInitiatedActiveTab: key
            })
        }
    }

    addRefOffset = (element, key) => {
        const moduleRefs = cloneDeep(this.state.moduleRefs)

        if(!element){
            return 
        }
        if(!moduleRefs[key]){
            const offset = element.getBoundingClientRect().top

            moduleRefs[key] = {
                element,
                offset
            }

            this.setState({
                moduleRefs
            })
        }
    }

    render() {
        const { 
            post,  
        } = this.props

        const {
            moduleRefs,
            isVisible
        } = this.state

        if (!post || !post.title) return <Error statusCode={404} />

        let mainClassName = 'main-container'

        if(isVisible){
            mainClassName += ' visible'
        }

        const pages = post.acf.pages

        const primaryColour = { color: post.acf.primary_colour }
        const secondaryColour = { color: post.acf.secondary_colour }

        const elementProps = {
            primaryColour,
            secondaryColour,
            client: post,
            pages,
            scrollContainerRef: this.scrollContainerRef,
            getNormalisedActiveTab: this.getNormalisedActiveTab,
            parentState: this.state,
            setActiveTab: this.setActiveTab
        }

        return (
            <Layout>
                <div className={mainClassName}>
                    <Sidebar 
                        moduleRefs={moduleRefs} 
                        { ...elementProps }
                    />
                    <Content 
                        addRefOffset={this.addRefOffset} 
                        { ...elementProps }
                    />
                </div>
            </Layout>
        );
    }
}

export default Main