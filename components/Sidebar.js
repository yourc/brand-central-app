import React,  { Component } from 'react';
import Scroll from 'react-scroll'
import { forIn } from 'lodash'
import Config from '../config';
import Router from 'next/router';
import { SlideDown } from 'react-slidedown'
import 'react-slidedown/lib/slidedown.css'

const hrStyle = {
  marginTop: 75,
};

class Sidebar extends Component {
    scroll = Scroll.animateScroll
    username = ''
    menuItemRefs = {}
    highlightRef = null
    normalisedActiveKey = 0


    state = {
        activeKey: 0,
        menuVisible: false,
        highlightYPos: 170,
        userInitiatedScrollKey: null
    }

    componentDidMount() {
        document.getElementById("scroll-container").addEventListener('scroll', this.handleScroll)
    }

    componentWillUnmount() {
        document.getElementById("scroll-container").removeEventListener('scroll', this.handleScroll)
    }

    componentWillUpdate = (prevProps, prevState) => {
        this.calculateActiveTab()
    }

    componentDidUpdate = prevProps => {
        const oldParentState = prevProps.parentState
        const newParentState = this.props.parentState

        if(!oldParentState.userInitiatedActiveTab && !newParentState.userInitiatedScrollKey){
            if(oldParentState.scrolledActiveTab.length == 3){
                if(newParentState.scrolledActiveTab > oldParentState.scrolledActiveTab){
                    this.setState({
                        shouldAccountForShrunkMenuItem: true
                    })

                    setTimeout(_ => {
                        this.setState({
                            shouldAccountForShrunkMenuItem: false
                        })
                    },300)
                }
            }
        }
    }

    handleScroll = _ => {
        const {
            parentState,
            getNormalisedActiveTab
        } = this.props

        if(parentState.userInitiatedActiveTab){
            return 
        }

        this.calculateActiveTab()

        const activeTab = getNormalisedActiveTab()

        const activeMenuItemRef = this.menuItemRefs[activeTab]

        if(activeMenuItemRef){
            const highlightYPos = activeMenuItemRef.getBoundingClientRect().top

            if(highlightYPos != this.state.highlightYPos){
                this.setState({
                    highlightYPos
                })
            }
        }
    }

    setChapter = (key, isSubChapter) => {
        const {
            moduleRefs,
            getNormalisedActiveTab,
            setActiveTab
        } = this.props

        let shouldAccountForShrunkMenuItem = false

        if(isSubChapter){
            const oldKey = getNormalisedActiveTab()

            if(oldKey.length == 3 && key.length == 3){
                if(key > oldKey){
                    console.log(`The menu item will shrink, affecting the offset. Adjusting accordingly...`)
                    shouldAccountForShrunkMenuItem = true
                }
            }
        }

        const thisOffset = moduleRefs[key].offset

        this.scroll.scrollTo(thisOffset, { 
            duration: 500,
            smooth: true,
            containerId: 'scroll-container',
        })

        this.setState({
            shouldAccountForShrunkMenuItem
        })

        setActiveTab(key, 'user')

        setTimeout(_ => {
            setActiveTab(null, 'user')

            this.setState({
                shouldAccountForShrunkMenuItem: false
            })
        },500)
    }

    calculateActiveTab = _ => {
        const {
            moduleRefs,
            setActiveTab,
            parentState
        } = this.props

        let newActiveKey = 0

        if(typeof window !== 'undefined'){
            const scrollContainer = document.getElementById('scroll-container')
            const scrollHeight = scrollContainer.getBoundingClientRect().height
            const scrollTop = scrollContainer.scrollTop + (scrollHeight / 2) // midpoint of screen

            forIn(moduleRefs, (object, key) => {
                if(scrollTop > object.offset && scrollTop <= object.offset + object.element.getBoundingClientRect().height){
                    newActiveKey = key
                }
            })
            
            if(newActiveKey !== parentState.scrolledActiveTab){
                setActiveTab(newActiveKey)
            }
        }
    }

    toggleMenu = _ => {
        this.setState({
            menuVisible: !this.state.menuVisible
        })
    }

    logout = _ => {
        const {
            client,
        } = this.props

        const clientSlug = client.slug

        localStorage.removeItem(Config.AUTH_TOKEN)
        Router.push('/'+clientSlug)
    }

    getHighlightYPos = _ => {
        const {
            getNormalisedActiveTab
        } = this.props

        let key = getNormalisedActiveTab()

        const {
            shouldAccountForShrunkMenuItem
        } = this.state

        const activeRef = this.menuItemRefs[key]
        const sidebarRef = this.sidebarRef

        if(!activeRef){
            return 0
        }

        let highlightYPos = activeRef.getBoundingClientRect().top + sidebarRef.scrollTop
        
        if(shouldAccountForShrunkMenuItem){
            highlightYPos -= 10
        }

        return highlightYPos
    }

    isActive = _testKey => {
        const {
            getNormalisedActiveTab
        } = this.props

        let normalisedActiveKey = getNormalisedActiveTab()

        if(!normalisedActiveKey){
            return 
        }

        normalisedActiveKey = normalisedActiveKey.toString()

        const testKey = _testKey.toString()

        return (testKey == normalisedActiveKey) && testKey.length == normalisedActiveKey.length
    }

    render() {
        const {
            pages,
            client,
            primaryColour,
            getNormalisedActiveTab
        } = this.props 

        const highlightYPos = this.getHighlightYPos()

        const highlightStyle = { transform: `translateY(${highlightYPos}px)` }

        let highlightClassName = 'highlight'
        let normalisedActiveKey = getNormalisedActiveTab()

        if(normalisedActiveKey && normalisedActiveKey.toString().length == 1){
            highlightClassName += ' heading'
        }

        return (
            <div 
                className="sidebar-container" 
                id="sidebar-container"
                ref={ref => this.sidebarRef = ref}
            >
                <div className="header-block">
                    <img src="https://tedxmelbourne.com/wp-content/themes/tedx/img/logo-yc-footer.png"/>
                    <div className="account">
                        <h4 style={primaryColour}>
                            Brand Central
                        </h4>
                        <p>
                            {client.title.rendered}
                        </p>
                    </div>
                </div>
                <div className={highlightClassName} style={highlightStyle} ref={ref => this.highlightRef = ref}>
                    <div className="square top"/>
                    <div className="square bottom"/>
                </div>
                <ul>
                    {pages.map((page, pageIndex) => {
                        const isTitlePageActive = this.isActive(pageIndex)

                        if(!page.sections){
                            console.log('no sections')
                            console.log(page)
                            return null
                        }
                        
                        return (
                            <li key={pageIndex}
                                className={isTitlePageActive ? 'page active' : 'page'}
                                ref={ref => {this.menuItemRefs[pageIndex] = ref}}
                            >
                                <button 
                                    onClick={_ => {this.setChapter(pageIndex)}}
                                >
                                    <h3>
                                        { page.title }  
                                    </h3>
                                </button>
                                <ul>
                                    {page.sections.map((section, sectionIndex) => {
                                        const key = pageIndex+'.'+sectionIndex
                                        const isSectionActive = this.isActive(key)

                                        return (
                                            <li 
                                                key={sectionIndex} 
                                                className={isSectionActive ? 'section active' : 'section'}
                                                ref={ref => {this.menuItemRefs[key] = ref}}
                                            >
                                                <button onClick={_ => {this.setChapter(key, true)}}>
                                                    {section.section_title}
                                                </button>
                                            </li>
                                        )
                                    })}
                                </ul>
                            </li>   
                        )
                    })}
                </ul>
                <button className="logout" onClick={_ => (this.logout())}>
                    Logout
                </button>
            </div>
        )
    }
}

export default Sidebar;
