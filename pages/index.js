import React, { Component } from 'react';
import Link from 'next/link';
import Router from 'next/router';
import WPAPI from 'wpapi';
import Layout from '../components/Layout';
import Config from '../config';

const wp = new WPAPI({ endpoint: Config.apiUrl })

wp.clients = wp.registerRoute('wp/v2', '/clients/(?P<id>)')

const headerImageStyle = {
    marginTop: 50,
    marginBottom: 50,
};

const tokenExpired = () => {
    if (process.browser) {
        localStorage.removeItem(Config.AUTH_TOKEN);
    }
    wp.setHeaders('Authorization', '');
    Router.push('/login');
};

class Index extends Component {
    state = {
        id: '',
    };

    static async getInitialProps() {
        try {
            const [page, posts, pages, clients] = await Promise.all([
                wp
                    .pages()
                    .slug('welcome')
                    .embed()
                    .then(data => {
                        return data[0];
                    }),
                wp.posts().embed(),
                wp.pages().embed(),
                wp.clients().embed()
            ]);

            return { page, posts, pages, clients }
        } catch (err) {
            console.log(err)
            if (err.data.status === 403) {
                tokenExpired();
            }
        }

        return null;
    }

    componentDidMount() {
        const token = localStorage.getItem(Config.AUTH_TOKEN);
        
        if (token) {
            wp.setHeaders('Authorization', `Bearer ${token}`);
            wp.users()
                .me()
                .then(data => {
                    const { id } = data;
                    this.setState({ id });
                })
                .catch(err => {
                    if (err.data.status === 403) {
                        tokenExpired();
                    }
                });
        }
    }

    render() {
        const { id } = this.state

        const {
            headerMenu,
            page,
            clients
        } = this.props

        return (
            <Layout>
                <h1>
                    Ignore this page for now.
                </h1>
                <h3>
                    Clients:
                </h3>
                <ul>
                    {clients.map(client => {
                        return (
                            <li key={client.slug}>
                                <Link
                                    as={`/${client.slug}`}
                                    href={`/clients?slug=${client.slug}&apiRoute=client`}
                                >
                                    <a>{client.title.rendered}</a>
                                </Link>
                            </li>
                        )
                    })}
                </ul>
                {id ? (
                    <div>
                        <h2>You Are Logged In</h2>
                        <p>
                            Your user ID is <span>{id}</span>, retrieved via an authenticated
                            API query.
                        </p>
                    </div>
                ) : (
                        <div>
                            <h2>You Are Not Logged In</h2>
                            <p>
                                The frontend is not making authenticated API requests.{' '}
                                <a href="/login">Log in.</a>
                            </p>
                        </div>
                    )}
            </Layout>
        );
    }
}

export default Index