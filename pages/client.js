import React, { Component } from 'react';
import Error from 'next/error';
import WPAPI from 'wpapi';
import Layout from '../components/Layout';
import Config from '../config';
import Router from 'next/router';
import axios from 'axios';
import {SlideDown} from 'react-slidedown'
import 'react-slidedown/lib/slidedown.css'
import Link from 'next/link';

const wp = new WPAPI({ endpoint: Config.apiUrl })

wp.clients = wp.registerRoute('wp/v2', '/clients/(?P<id>)')

class Client extends Component {

    state = {
        username: '',
        password: '',
        message: '',
        hasLoaded: false
    }

    static async getInitialProps(context) {
        const { slug, apiRoute } = context.query;

        let apiMethod = wp.clients();

        const post = await apiMethod
            .slug(slug)
            .embed()
            .then(data => {
                return data[0];
            });

        return {
            post,
            slug,
        }
    }

    login() {
        console.log(`For fuck's fucking sake...`)

        let message = '';
        this.setState({ message });
        const { username, password } = this.state;
        axios
            .post(`${Config.apiUrl}/jwt-auth/v1/token`, {
                username,
                password,
            }, 
            {
                headers: {
                    "Content-Type": "application/json"
                }
            })
            .then(res => {
                console.log(res)

                const { data } = res;
                localStorage.setItem(Config.AUTH_TOKEN, data.token);
                localStorage.setItem(Config.USERNAME, data.user_nicename);

                const clientSlug = this.props.post.slug

                const token = localStorage.getItem(Config.AUTH_TOKEN); 

                const username = localStorage.getItem(Config.USERNAME); 

                function parseJwt(token) {
                    if (!token) { return; }
                    const base64Url = token.split('.')[1];
                    const base64 = base64Url.replace('-', '+').replace('_', '/');
                    return JSON.parse(window.atob(base64));
                }
                
                const parsedToken = parseJwt(token)

                const userId = parsedToken.data.user.id 
                
                wp.setHeaders('Authorization', `Bearer ${token}`)
                wp.users()
                    .me()
                    .then(data => {
                        const { id } = data
                        this.setState({ id })

                        const clients = data.acf.clients 

                        if(!clients){
                            return
                        }

                        const clientId = this.props.post.id

                        const clientIndexIfExists = clients.findIndex(client => {
                            return client.client.ID === clientId
                        })

                        if(clientIndexIfExists !== -1) {
                            setTimeout(() => {
                                this.setState({
                                    hasLoaded: false
                                })
                            },100)

                            return Router.push(`/${clientSlug}/main`)
                        }

                        return this.handleLoginError(403)
                    })
            })
            .catch(e => {
                this.handleLoginError(422)
            })
    }

    handleLoginError = type => {
        let message = null

        switch(type){
            case 422:
                message = `Sorry, that username or password is incorrect.`
                break
            case 403:
                message = `Sorry, you don't have access to this client.`
                break
            case 401:
                message = `Sorry, you'll need to login first.`
                break
        }

        this.setState({
            message
        })

        setTimeout(_ => {
            this.setState({
                message: null
            })
        },2500)
    }

    handleKeyDown = (e) => {
        if (e.key === 'Enter') {
          this.login()
        }
    }

    componentDidMount() {
       setTimeout(() => {
            this.setState({
                hasLoaded: true
            })
    
            setTimeout(_ => {
                const fullUrl = this.props.url.asPath 
        
                const queryString = fullUrl.split('?')[1]
        
                if(queryString == 'access=denied') {
                    this.handleLoginError(401)
                }
            },700)
       },100)

    }

    render() {
        const {
            post,
            headerMenu,
            slug
        } = this.props

        const {
            hasLoaded
        } = this.state

        if (!post || !post.title) return <Error statusCode={404} />;

        const { username, password, message } = this.state;

        const backgroundImageStyle = { backgroundImage: `url(${post.acf.background_image})` }

        let screenClassName = 'login-screen'

        if(hasLoaded){
            screenClassName += ' loaded'
        }

        const hasColours = post.acf && post.acf.primary_colour && post.acf.secondary_colour

        return (
            <Layout>
                { hasColours ?
                    <style>
                        {`
                            .login-screen  .form-container input:focus{
                                border-color: ${ post.acf.primary_colour };
                            }

                            .login-screen .form-container form button {
                                background: ${ post.acf.primary_colour };
                            }
                        `}
                    </style>
                : null }
                <div className={screenClassName}>
                    <div className="bg" style={backgroundImageStyle} />
                    <div className={'form-container'}>
                        <form>
                            <img className="logo" src={post.acf.logo} />
                            <h1>
                                Welcome to Brand Central
                            </h1>
                            <input
                                className="input-padding"
                                value={username}
                                onChange={e => this.setState({ username: e.target.value })}
                                type="text"
                                placeholder="Your username"
                                onKeyDown={this.handleKeyDown}
                            />
                            <input
                                className="input-padding"
                                value={password}
                                onChange={e => this.setState({ password: e.target.value })}
                                type="password"
                                placeholder="Your password"
                                onKeyDown={this.handleKeyDown}
                            />
                            <button
                                className="button"
                                type="button"
                                onClick={() => {
                                    this.login();
                                }}
                            >
                                Login
                            </button>
                        </form>
                        <SlideDown className={'my-dropdown-slidedown'}>
                            <p>
                                {this.state.message ? this.state.message : null}
                            </p>
                        </SlideDown>
                    </div>
                </div>
            </Layout>
        );
    }
}

export default Client;
