import React, { Component } from 'react';
import ImageModule from './ImageModule'
import TypeModule from './TypeModule'
import ColourModule from './ColourModule'
import WysiwygModule from './WysiwygModule'

const hrStyle = {
    marginTop: 75,
};

class Content extends Component {
    state = {
        hasMountedWithDelay: false
    }

    componentDidMount() {
        setTimeout(_ => {
            this.setState({
                hasMountedWithDelay: true
            })
        },1200)
    }
    
    render() {
        const {
            hasMountedWithDelay
        } = this.state

        const {
            pages,
            addRefOffset,
            primaryColour,
            secondaryColour,
            client,
            getNormalisedActiveTab,
        } = this.props

        const activeTab = getNormalisedActiveTab()

        return (
            <div className="content-container" id="scroll-container">
                <style>
                    {`
                        @font-face {
                            font-family: 'custom';
                            src: url('${client.acf.light_font['url']}') format('woff');
                            font-style: normal;
                            font-weight: 300;
                        }
                        
                        @font-face {
                            font-family: 'custom';
                            src: url('${client.acf.normal_font['url']}') format('woff');
                            font-style: normal;
                            font-weight: 500;
                        }

                        @font-face {
                            font-family: 'custom';
                            src: url('${client.acf.bold_font['url']}') format('woff');
                            font-style: normal;
                            font-weight: 700;
                        }

                        .content-container {
                            font-family: 'custom';
                        }
                    `}
                </style>
                <ul className="pages">
                    {pages.map((page, pageIndex) => {
                        if(!page.sections){
                            console.log('no sections')
                            console.log(page)
                            return null
                        }

                        let sectionStyle = null 

                        page.background_image ? sectionStyle = { backgroundImage: `url(${page.background_image})` } : sectionStyle = { background: page.background_colour }

                        const headingStyle = {color: `${page.text_colour}`}

                        let titlePageClassName = 'title'

                        const isTitlePageScrolledIntoView = activeTab.toString() === pageIndex.toString()

                        if(isTitlePageScrolledIntoView && hasMountedWithDelay){
                            titlePageClassName += ' active'
                        }

                        return (
                            <li className="page" key={pageIndex} ref={ref => {
                                if (ref) {
                                    addRefOffset(ref, pageIndex)
                                }
                            }}>

                                <section
                                    className={titlePageClassName}
                                    style={sectionStyle}
                                >
                                    <div>
                                        <h1 style={headingStyle}>
                                            {page.title}
                                        </h1>
                                        <h3 style={headingStyle}>
                                            {page.blurb}
                                        </h3>
                                    </div>
                                </section>

                                <ul className="page-sections">
                                    {page.sections.map((section, sectionIndex) => {
                                        const thisSlug = section.acf_fc_layout

                                        const data = section

                                        let key = pageIndex + '.' + sectionIndex
                                        let isScrolledIntoView = false

                                        if(activeTab.toString() == key){
                                            isScrolledIntoView = true
                                        }

                                        let sectionStyle = data.background_colour ? {background: data.background_colour} : null
                                        let actualPrimaryColour = null 
                                        let actualSecondaryColour = null

                                        if(data.text_colour) {
                                            actualPrimaryColour = {color: data.text_colour}
                                            actualSecondaryColour = {color: data.text_colour}
                                        } else {
                                            actualPrimaryColour = primaryColour
                                            actualSecondaryColour = secondaryColour
                                        }

                                        let Module = null 

                                        switch(thisSlug){
                                            case 'image':
                                                Module = ImageModule
                                                break
                                            case 'colour':
                                                Module = ColourModule
                                                break
                                            case 'type':
                                                Module = TypeModule
                                                break
                                            case 'wysiwyg':
                                                Module = WysiwygModule
                                                break
                                        }

                                        const moduleProps = {
                                            primaryColour,
                                            secondaryColour,
                                            data
                                        }

                                        let className = thisSlug + ' section'

                                        if(isScrolledIntoView){
                                            className += ' active'
                                        }

                                        return (
                                            <li
                                                className={className}
                                                style={sectionStyle}
                                                key={sectionIndex}
                                                ref={ref => {
                                                    if (ref) {
                                                        addRefOffset(ref, key)
                                                    }
                                                }}
                                            >
                                                <div className="header-container">
                                                    <div className="column left">
                                                        {data.heading ? 
                                                            <h1 style={actualPrimaryColour}>
                                                                {data.heading}
                                                            </h1>
                                                        : null
                                                        }
                                                        <h2 style={actualSecondaryColour}>
                                                            { page.title }
                                                        </h2>
                                                    </div>
                                                    <div className="column right">
                                                        {data.copy ? 
                                                            <p style={secondaryColour}>
                                                                {data.copy}
                                                            </p>
                                                        : null 
                                                        }
                                                    </div>
                                                </div>
                                                <Module
                                                    { ...moduleProps }
                                                />
                                            </li>
                                        )

                                    })}

                                </ul>

                            </li>
                        )
                    })}
                </ul>
            </div>
        )
    }
}

export default Content;
