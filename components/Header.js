import React from 'react';
import Head from 'next/head';
import stylesheet from '../src/styles/style.sass';

const Header = () => (
  <div>
    <Head>
      <style
        // eslint-disable-next-line react/no-danger
        dangerouslySetInnerHTML={{ __html: stylesheet }}
      />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <meta charSet="utf-8" />
      <title>Brand Central</title>
      <link rel="stylesheet" target="_blank" href="https://use.typekit.net/chz6pxs.css"/>
    </Head>
  </div>
);

export default Header;
