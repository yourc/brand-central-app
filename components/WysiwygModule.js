import React,  { Component } from 'react';

const hrStyle = {
  marginTop: 75,
};

class WysiwygModule extends Component {
    render() {
        const {
            data,
            primaryColour,
            secondaryColour
        } = this.props

        let className = ''

        if(!data.columns){
            return null
        }

        if(data.columns.length % 1 == 0) {
            className = 'single'
        }

        if(data.columns.length % 2 == 0) {
            className = 'double'
        }

        if(data.columns.length % 3 == 0) {
            className = 'thirds'
        }

        return (
            <div className="module-container">
                <ul className={className}>
                    {data.columns.map(column => {
                        return (
                            <li>
                                 <span style={primaryColour} className="wysiwyg" dangerouslySetInnerHTML={{__html: column.wysiwyg}} />
                            </li>
                        )
                    })}
                </ul>
                {
                    data.file_download ? 
                        <a className="button" 
                            href={data.button_file}
                        >
                            {data.button_label}
                        </a>
                    : null
                }
            </div>
        )
    }
}

export default WysiwygModule;